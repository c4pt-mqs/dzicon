#!/usr/bin/env python3

import os
import sys
import deepzoom

# Check if the filename argument is provided
if len(sys.argv) < 2:
    print("Please provide one or more filenames as arguments.")
    sys.exit(1)

# Get the filenames from the command-line arguments
filenames = sys.argv[1:]

for filename in filenames:
    # Check if the file exists
    if not os.path.exists(filename):
        print(f"\nFile '{filename}' does not exist.\nUsage:\n\t./dzi_converter.py [filename1.png] [filename2.jpg]\n\tpython3 dzi_converter.py [filename.jpg]\n")
        continue

    # Determine the file format based on the file extension
    file_extension = os.path.splitext(filename)[1].lower()
    if file_extension == '.jpg' or file_extension == '.jpeg':
        tile_format = 'jpg'
    elif file_extension == '.png':
        tile_format = 'png'
    else:
        print(f"Unsupported file format: '{file_extension}'. Supported formats are JPEG (.jpg, .jpeg) and PNG (.png).")
        continue

    # Create Deep Zoom Image creator with desired parameters
    creator = deepzoom.ImageCreator(
        tile_size=512,
        tile_overlap=2,
        tile_format=tile_format,
        image_quality=1,
        resize_filter="bicubic",
    )

    # Create Deep Zoom image pyramid from source
    output_dir = os.path.splitext(filename)[0]
    creator.create(filename, f"{output_dir}.dzi")
    print(f"Image '{filename}' converted successfully.")
